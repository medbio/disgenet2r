# disgenet2r

`disgenet2r`: An R package to explore DISGENET

## Package' Status

 * __Version__: 1.2.2
 * __Subversion__: `20241209`
 * __Authors__:  MEDBIOINFORMATICS SOLUTIONS SL
 * __Maintainer__: <janet.pinero@disgenet.com>

## How to start

### Installation

The package, `disgenet2r` can be installed using `devtools` from this repository:

```R
library(devtools)
install_gitlab("medbio/disgenet2r")
```

### Obtaining the API key

Before using the package, you will need to register for a DISGENET account at [https://www.disgenet.com/](https://www.disgenet.com/). Once you have completed the registration process, retrieve your API key in your user profile.
After retrieving the API key, run the line below so the key is available for all the **disgenet2r** functions. 

```{r, warning = FALSE, message = FALSE, eval=T}
library(disgenet2r)
api_key <- "enter your API key here"
Sys.setenv(DISGENET_API_KEY= api_key)

```


### Querying DISGENET data:

The following lines show two examples of how DISGENET can be queried using `disgenet2r`:

 * __Gene Query__

```R
res <- gene2disease(gene = 3953, 
 vocabulary = "ENTREZ",
    database = "ALL", 
    score = c( 0.1,1)
)

```

 * __Disease Query__

```R
res <- disease2gene(disease = "UMLS_C0028754", 
    database = "ALL",
    score = c(0.3,1) 
)
```


 * __Variant Query__

```R
res <-  variant2disease(variant = "rs1800629",
       database = "ALL",
         score =c(0,1)
)
```

A detailed documentation of the package is available [here](https://medbio.gitlab.io/disgenet2r/).



## COPYRIGHT

©2024 MedBioinformatics Solutions SL


## License

disgenet2r is distributed under the GPL-2 license. 


 

