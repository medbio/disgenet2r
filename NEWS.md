# disgenet2r 1.2.2

## New features
- The **disgenet_enrichment** function now returns the genes or variants that are common between the user’s list and the genes/variants associated to the significant disease (intersection).
- The **disgenet_enrichment** function now receives a `disList` parameter to restrict the enrichment to the list of diseases supplied by the user. 

# disgenet2r 1.2.1

## New features
- The **disgenet_enrichment** function now accepts lists of up to 4,000 entities. 
- The result of the **disgenet_enrichment** function can be visualized using the **plot** function with the argument *type="Enrichment"*.   

## Refactoring
- Better handling of messages related to `order_by` parameters.
- Improved documentation of  `order_by`.

# disgenet2r 1.2

## New functions

-   **disgenet_enrichment**, that performs an disease enrichment analysis on a list of genes or variants.
-   **gene2vda** that receives genes to retrieve VDAs using HGNC symbols, ENSEMBL genes, UNIPROT accessions, and ENTREZ identifiers.

## New features

-   functions **disease2gene**, **disease2variant**, **disease2evidence**, and **disease2disease** accept diseases coded using MONDO identifiers.
-   functions **gene2disease**, **gene2disease** , **gene2chemical** and **gene2attribute** accept UNIPROT accessions.
-   functions **gene2chemical** and **disease2chemical** are faster

# disgenet2r 1.1.5

## New features

-   functions **gene2disease**, **disease2gene**, **variant2disease**, **disease2variant**, **chemical2gda**, **chemical2vda**, now receive a new parameter, `order_by` to sort results, that can take the following values: score, dsi, dpi, pli, ei, yearInitial, yearFinal, numCTsupportingAssociation

-   functions **gene2evidence**, **disease2evidence**, **variant2evidence**, **gene2chemical**, **disease2chemical**, **variant2chemical**, now receive a new parameter, `order_by` to sort results, that can take the following values: score, dsi, dpi, pli, pmYear

## Refactoring

-   Better handling of messages related to page numbers, and number of pages.
-   Refactoring of the main function that retrieves data from the API (**get_data_api**).

# disgenet2r 1.1.4

## New Features

-   Data for clinical trials can be retrieved in **gene2disease** and **disease2gene** functions.
-   Plots available for disease-disease-rela and disease-disease-sokal objects

## Refactoring

All functions return the number of pages if parameter verbose is set to TRUE. Warnings are shown if the number of pages of a query is larger than 100.

# disgenet2r 1.1.3

## Bug fixes

Heatmap of Protein and Disease classes error fixed to work in R 4.x versions

# disgenet2r 1.1.2

## Bug fixes

Better handling of empty results for all queries

# disgenet2r 1.1.1

## Refactoring

All functions were refactored to properly handle access features according to the user’s profile.

# disgenet2r 1.1

## Refactoring

All functions that contained disgenetplus2r have been renamed to disgenet2r. All documentation and mentions of disgenet plus have been updated to disgenet. The function \*\*get_disgenet_api_key.R\* was removed

# disgenetplus2r 1.0.7

## Bug fixes

an url encoder function was added to the **get_disgenet_api_key** to avoid problems with passwords that contain "&".

# disgenetplus2r 1.0.6

## New Features

-   Functions **gene2chemical**,**disease2chemical** ,**variant2chemical** ,and **chemical2evidence**, now return a new field, `chemical_effect`, with possible values: toxicity, therapeutic and unrelated

# disgenetplus2r 1.0.5

## Bug fixes

-   Fixed functions chemical2disease, chemical2gene, and chemical2variant, that produced repeated results.

# disgenetplus2r 1.0.4

## New Features

-   DiseaseClass plots are possible for objects of type chemical-disease
-   ProteinClass plots are possible for objectos of type chemical-gene
-   Protein Class and Disease Class plots for single entity searches show node sizes proportional to the number of proteins/diseases in class, for both, interactive and non-interactive plots
-   added new parameter (eprop) to network plots that allows to manipulate edge width

## Refactoring

-   plot error messages have all the same format

## Bug fixes

-   fixed protein class plots that showed empty values as nodes

# disgenetplus2r 1.0.3

## New Features

-   Queries with functions **disease2variant**, **chemical2variant**, and **chemical2vda** and now can receive SIFT and POLYPHEN ranges

## Documentation

-   NEWS.md file was added to document package changes.
-   all functions that use the parameter `database` now have all datasources, including RGD and MGD, that are new sources

## Bug fixes

-   Functions that use the parameter `database` that query variant endpoints now throw an error if a database is not GWASCAT, CLINVAR, UNIPROT, TEXTMINING_HUMAN, CURATED, or ALL.
