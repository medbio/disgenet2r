#' Retrieves diseases similar to an input disease and generates an \code{DataGeNET.DGN}
#'
#' Given one or multiple diseases retrieves diseases similar to the query disease(s)
#' from DISGENET and creates an object of type \code{DataGeNET.DGN}.
#'
#' @param disease a disease, or list of diseases using the following identifiers:
#' UMLS, ICD9CM, ICD10, MESH, OMIM, DO, EFO, NCI, HPO, MONDO, or ORDO.
#' The diseases non contained in DISGENET will
#' be removed from the output.
#' @param min_sokal A minimum value for the Sokal-Sneath semantic similarity distance. Default \code{0.1}
#' @param verbose Logical; by default \code{FALSE}. Set to \code{TRUE} to enable real-time logging from the function.
#' @param warnings By default \code{TRUE}. Set to \code{FALSE} to hide the warnings.
#' @param n_pags A number between 0 and 100 indicating the number of pages to retrieve from the results of the query. Default \code{100}
#' @param api_key An api key to connect to DISGENET.
#' @return An object of class \code{DataGeNET.DGN}
#' @examples
#' dis_res <- get_similar_diseases( "UMLS_C0028754" )
#' @export get_similar_diseases

get_similar_diseases <- function(disease, api_key=NULL, n_pags = 5, min_sokal=0.1, verbose = FALSE, warnings = TRUE  ) {


  if(is.null(api_key)){
    api_key = Sys.getenv('DISGENET_API_KEY')
    if(api_key == ""){
      stop("This is not a valid API KEY!")
    }
  }
  if(length( disease )==1){
    cls <- "single"
  }else {
    cls <- "list"
  }

  list_of_diseases <- paste(unique(disease),collapse=",")

  url <- paste0( get_url_disgenet(), "dda?disease=", list_of_diseases ,
                 "&min_sokal=",min_sokal, "&order_by=SOKAL")
  data_api  <- get_data_api(url = url, n_pags = n_pags, verbose=verbose)
  result<- data_api[[2]]


  if ( nrow(result) != 0){
    result$diseaseid1 <-gsub("^.*UMLS_(C\\d{7})", "\\1", result$disease1_Vocabularies)
    result$diseaseid1 <-gsub("\\)", "", result$diseaseid1)
    result$diseaseid1 <-gsub('\\"', "", result$diseaseid1)

    result$diseaseid2 <-gsub("^.*UMLS_(C\\d{7})", "\\1", result$disease2_Vocabularies)
    result$diseaseid2 <-gsub("\\)", "", result$diseaseid2)
    result$diseaseid2 <-gsub('\\"', "", result$diseaseid2)


    codes1 <- tidyr::unnest(result[, c("diseaseid1","disease1_Vocabularies")], "disease1_Vocabularies")
    codes2 <- tidyr::unnest(result[, c("diseaseid2","disease2_Vocabularies")], "disease2_Vocabularies")
    colnames(codes2) <- colnames(codes1)
    codes <- rbind(codes1, codes2)
    found_diseases <- intersect(disease, codes$disease1_Vocabularies)
    missing_diseases <- setdiff(disease, codes$disease1_Vocabularies)
    cuis <- unique(codes[ codes$disease1_Vocabularies %in% disease, ]$diseaseid1)
    result$query <- ifelse(result$diseaseid1 %in% cuis, result$diseaseid1, result$diseaseid2)
    if( length( missing_diseases ) > 0 ) {
      diseases <- paste( paste( "   -", missing_diseases ), collapse = "\n" )
      if( warnings ) {
        warning( "\n One or more diseases in the list is not in DISGENET:\n", diseases )
      }
    }
    result <- result %>% dplyr::select(-"assocID")
    result$sokal <- round(result$sokal,   digits = 3)

    result <- result[ ,  c("diseaseid1","disease1_Name", "disease1_Type", "disease1_Classes_MSH",
                           "diseaseid2", "disease2_Name","disease2_Type", "disease2_Classes_MSH",
                           "sokal", "query")]
    result <- unique(result)

    result <-  methods::new( "DataGeNET.DGN",
                   type     = "disease-disease-sokal",
                   search   = cls,
                   term       = as.character( found_diseases ),
                   database = "ALL",
                   scoreRange =  "",
                   qresult  = result
    )

    return( result )
  } else{
    message(
      "There are no results for those parameters "
    )
  }

}
