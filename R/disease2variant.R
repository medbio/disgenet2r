#' Retrieves the variants associated to a disease, or list of diseases and generates an \code{DataGeNET.DGN}
#'
#' Given one or multiple diseases retrieves their associated variants
#' from DISGENET and creates an object of type \code{DataGeNET.DGN}.
#' @param disease a disease, or list of diseases using the following identifiers:
#' UMLS, ICD9CM, ICD10, MESH, OMIM, DO, EFO, NCI, HPO, MONDO, or ORDO.
#' The diseases non contained in DISGENET will
#' be removed from the output.
#' @param database Name of the database that will be queried. It can take the values:
#' \code{'CLINVAR'} to use ClinVar;
#' \code{'GWASCAT'} to use the NHGRI-EBI GWAS Catalog;
#' \code{'PHEWASCAT'} to use PHEWAS catalog;
#' \code{'UNIPROT'} to use Universal Protein Resource;
#' \code{'CURATED'} to use expert curated, human databases;
#' \code{'TEXTMINING_HUMAN'} to use text mining data, generated using own System;
#' \code{'ALL'} to use all these databases. Default \code{'CURATED'}.
#' @param score A vector with two elements: 1) initial value of score 2) final value of score
#' @param sift A vector with two elements: 1) initial value of sift 2) final value of sift
#' @param polyphen A vector with two elements: 1) initial value of polyphen 2) final value of polyphen
#' @param verbose Logical; by default \code{FALSE}. Set to \code{TRUE} to enable real-time logging from the function.
#' @param order_by To order results by decreasing value of the parameter (default value: \code{score}).
#' it can take values: score, dsi, dpi, pli, ei, yearInitial, yearFinal, numCTsupportingAssociation, numPMIDs
#' @param warnings By default \code{TRUE}. Set to \code{FALSE} to hide the warnings.
#' @param n_pags A number between 0 and 100 indicating the number of pages to retrieve from the results of the query. Default \code{100}
#' @param api_key An api key to connect to DISGENET.
#' @return An object of class \code{DataGeNET.DGN}
#' @examples
#' dis2var <- disease2variant( disease = "UMLS_C0751955", database = "ALL", score=c(0,1) )
#' @export disease2variant


disease2variant <- function( disease, chemical=NA, database = "CURATED",  score=c(0,1), order_by ="score",
                             sift = NULL, polyphen =NULL,
                             api_key=NULL,  n_pags = 100, verbose = FALSE, warnings = TRUE ) {
  if(is.null(api_key)){
    api_key = Sys.getenv('DISGENET_API_KEY')
    if(api_key == ""){
      stop("This is not a valid API KEY!")
    }
  }
  check_disgenet_sources( database )
  curated_databases <- get_sources("vcurated")

  if (    !  database  %in% c( curated_databases, "CURATED" ,"TEXTMINING_HUMAN",  "ALL") ) {
    stop("This resource does not have variant-disease associations.")
  }


  if( length( disease ) != length( unique( disease ) ) ) {
    disease <- unique( disease )
    warning(
      "Removing duplicates from input list."
    )
  }
  list_of_diseases <- paste(disease,collapse=",")

  if(length(score) != 2) {
    stop("Invalid argument 'score'. It must have two elements: initial value of the score, and final value of the score")
  }
  if (!is.null(sift) & !is.null(polyphen)) {
    if(length(sift) != 2) {
        stop("Invalid argument 'sift'. It must have two elements: initial value of the sift parameter, and final value of the sift parameter")
    }
    if(length(polyphen) != 2) {
      stop("Invalid argument 'polyphen'. It must have two elements: initial value of the polyphen parameter, and final value of the polyphen parameter")
    }
    url <- paste0( get_url_disgenet(), "vda/summary?disease=", list_of_diseases , "&source=",database,
                                "&min_score=",score[1],"&max_score=", score[2],
                                "&min_polyphen=", polyphen[1], "&max_polyphen=",polyphen[2],
                                "&min_sift=", sift[1], "&max_sift=",sift[2]  )
    }
  else if (!is.null(polyphen)) {
    if(length(polyphen) != 2) {
      stop("Invalid argument 'polyphen'. It must have two elements: initial value of the polyphen parameter, and final value of the polyphen parameter")
    }
    url <- paste0( get_url_disgenet(), "vda/summary?disease=", list_of_diseases , "&source=",database,
                   "&min_score=",score[1],"&max_score=", score[2],
                   "&min_polyphen=", polyphen[1], "&max_polyphen=",polyphen[2] )
  }
  else if (!is.null(sift)) {
    if(length(sift) != 2) {
      stop("Invalid argument 'sift'. It must have two elements: initial value of the sift parameter, and final value of the sift parameter")
    }
    url <- paste0( get_url_disgenet(), "vda/summary?disease=", list_of_diseases , "&source=",database,
                   "&min_score=",score[1],"&max_score=", score[2],
                   "&min_sift=", sift[1], "&max_sift=",sift[2])
  }
  else{
  url <- paste0( get_url_disgenet(), "vda/summary?disease=", list_of_diseases , "&source=",database, "&min_score=",score[1],"&max_score=", score[2])

  }
  check_disgenet_parameters(order_by, "summary")
  url <- paste0(url,  "&order_by=", order_by)

  academic_databases <- get_sources("academic_vdas")
  data_api  <- get_data_api(url = url, n_pags = n_pags, verbose=verbose)
  user_profile <- data_api[[1]]
  if(user_profile  == "ACADEMIC" & ! database %in% academic_databases){
    show_upgrade_message()
    stop()
  }
  if(user_profile  == "ACADEMIC" & ! is.na(chemical)){
    show_upgrade_message()
  }
  result<- data_api[[2]]
  if(nrow(result) != 0){
    colnames(result) <-gsub("geneSymbol_keywrod", "gene_symbol", colnames(result))
    colnames(result) <-gsub("geneNcbiIDs", "geneids", colnames(result))
    colnames(result) <-gsub("variantStrID", "variantid", colnames(result))

    result$diseaseid <-gsub("^.*UMLS_(C\\d{7})", "\\1", result$diseaseVocabularies)
    result$diseaseid <-gsub("\\)", "", result$diseaseid)
    result$diseaseid <-gsub('\\"', "", result$diseaseid)
    colnames(result) <-gsub("diseaseName", "disease_name", colnames(result))
    colnames(result)[which(colnames(result) == "el")] <- "evidence_level"
    colnames(result)[which(colnames(result) == "ei")] <- "evidence_index"

    if(length( disease )==1){
      cls <- "single"
    }else{
      cls <- "list"
    }
    codes <- tidyr::unnest(result[, c("diseaseid","diseaseVocabularies")], "diseaseVocabularies")

    found_diseases <- intersect(disease, codes$diseaseVocabularies)
    missing_diseases <- setdiff(disease, codes$diseaseVocabularies)

    if( length( missing_diseases ) > 0 ) {
      diseases <- paste( paste( "   -", missing_diseases ), collapse = "\n" )
      if( warnings ) {
        warning( "\n One or more diseases in the list is not in DISGENET ( '", database, "' ):\n", diseases )
      }
    }
    result <- result %>% dplyr::select(-"assocID")
    scR <- paste0( score[1],"-", score[2])


    result$score <- as.numeric(as.character(result$score))
    result <- methods::new( "DataGeNET.DGN",
                            type = "disease-variant",
                            search = cls,
                            term = as.character( found_diseases ),
                            scoreRange = scR,
                            database = database,
                            qresult = result )

    return( result )
  } else {
    print("no results for the query")
  }
}
